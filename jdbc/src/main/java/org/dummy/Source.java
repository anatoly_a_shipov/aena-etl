package org.dummy;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

/**
 * Source record.
 */
public class Source implements Serializable {

    static final long serialVersionUID = 1L;

    String actArrDateTimeLt;
    String aircraftNameScheduled;
    String arrAptNameEs;
    String arrAptCodeIata;
    String baggageInfo;
    String carrierAirlineNameEn;
    String carrierIcaoCode;
    String carrierNumber;
    String counter;
    String depAptNameEs;
    String depAptCodeIata;
    String estArrDateTimeLt;
    String estDepDateTimeLt;
    String flightAirlineNameEn;
    String flightAirlineName;
    String flightIcaoCode;
    String flightNumber;
    String fltLegSeqNo;
    String gateInfo;
    String loungeInfo;
    String schdArrOnlyDateLt;
    String schdArrOnlyTimeLt;
    String sourceData;
    String statusInfo;
    String terminalInfo;
    String arrTerminalInfo;
    Long createdAt;
    String actDepDateTimeLt;
    String schdDepOnlyDateLt;
    String schdDepOnlyTimeLt;

    /**
     * Constructor.
     */
    public Source() {
        //
    }

    public String stringFlight() {
        return new StringJoiner(", ", "{", "}")
                .add("flightIcaoCode='" + flightIcaoCode + "'")
                .add("flightNumber='" + flightNumber + "'")
                .add("schdDepOnlyDateLt='" + schdDepOnlyDateLt + "'")
                .add("schdDepOnlyTimeLt='" + schdDepOnlyTimeLt + "'")
                .toString();
    }

    /**
     * Flights.
     *
     * @param conn {@link Connection}
     * @return {@link List} {@link Source}
     */
    public static List<Source> flights(Connection conn) throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(
                    "SELECT DISTINCT FLIGHT_ICAO_CODE, FLIGHT_NUMBER, SCHD_DEP_ONLY_DATE_LT, SCHD_DEP_ONLY_TIME_LT"
                            + " FROM AENAFLIGHT_2017_01"
                            + " WHERE SCHD_DEP_ONLY_DATE_LT IS NOT NULL AND SCHD_DEP_ONLY_DATE_LT <> '' AND SCHD_DEP_ONLY_TIME_LT IS NOT NULL AND SCHD_DEP_ONLY_TIME_LT <> ''");
            if (rs.isBeforeFirst()) {
                List<Source> result = new ArrayList<>(0);
                Source source;
                while (rs.next()) {
                    source = new Source();
                    source.flightIcaoCode = rs.getString(1);
                    source.flightNumber = rs.getString(2);
                    source.schdDepOnlyDateLt = rs.getString(3);
                    source.schdDepOnlyTimeLt = rs.getString(4);
                    result.add(source);
                }
                if (!result.isEmpty()) {
                    return result;
                }
            }
            return Collections.emptyList();
        }
    }

    public static List<Source> sources(Connection conn, Source flight) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT  " +
                "ACT_ARR_DATE_TIME_LT, " +
                "AIRCRAFT_NAME_SCHEDULED, " +
                "ARR_APT_NAME_ES, " +
                "ARR_APT_CODE_IATA, " +
                "BAGGAGE_INFO, " +
                "COALESCE(CARRIER_AIRLINE_NAME_EN, '') AS CARRIER_AIRLINE_NAME_EN, " +
                "COALESCE(CARRIER_ICAO_CODE, '') AS CARRIER_ICAO_CODE, " +
                "COALESCE(CARRIER_NUMBER, '') AS CARRIER_NUMBER, " +
                "COUNTER, " +
                "DEP_APT_NAME_ES, " +
                "DEP_APT_CODE_IATA, " +
                "EST_ARR_DATE_TIME_LT, " +
                "EST_DEP_DATE_TIME_LT, " +
                "COALESCE(FLIGHT_AIRLINE_NAME_EN, '') AS FLIGHT_AIRLINE_NAME_EN, " +
                "FLIGHT_AIRLINE_NAME, " +
                "FLIGHT_ICAO_CODE, " +
                "FLIGHT_NUMBER, " +
                "FLT_LEG_SEQ_NO, " +
                "GATE_INFO, " +
                "LOUNGE_INFO, " +
                "SCHD_ARR_ONLY_DATE_LT, " +
                "SCHD_ARR_ONLY_TIME_LT, " +
                "SOURCE_DATA, " +
                "STATUS_INFO, " +
                "TERMINAL_INFO, " +
                "ARR_TERMINAL_INFO, " +
                "CREATED_AT, " +
                "ACT_DEP_DATE_TIME_LT, " +
                "SCHD_DEP_ONLY_DATE_LT, " +
                "SCHD_DEP_ONLY_TIME_LT  " +
                "FROM AENAFLIGHT_2017_01  " +
                "WHERE  " +
                "FLIGHT_ICAO_CODE = ? " +
                "AND FLIGHT_NUMBER = ? " +
                "AND SCHD_DEP_ONLY_DATE_LT = ? " +
                "AND SCHD_DEP_ONLY_TIME_LT = ? " +
                "ORDER BY CREATED_AT ASC")) {
            stmt.setString(1, flight.flightIcaoCode);
            stmt.setString(2, flight.flightNumber);
            stmt.setString(3, flight.schdDepOnlyDateLt);
            stmt.setString(4, flight.schdDepOnlyTimeLt);
            ResultSet rs = stmt.executeQuery();
            List<Source> result = new ArrayList<>(0);
            Source source;
            while (rs.next()) {
                source = new Source();
                source.actArrDateTimeLt = rs.getString(1);
                source.aircraftNameScheduled = rs.getString(2);
                source.arrAptNameEs = rs.getString(3);
                source.arrAptCodeIata = rs.getString(4);
                source.baggageInfo = rs.getString(5);
                source.carrierAirlineNameEn = rs.getString(6);
                source.carrierIcaoCode = rs.getString(7);
                source.carrierNumber = rs.getString(8);
                source.counter = rs.getString(9);
                source.depAptNameEs = rs.getString(10);
                source.depAptCodeIata = rs.getString(11);
                source.estArrDateTimeLt = rs.getString(12);
                source.estDepDateTimeLt = rs.getString(13);
                source.flightAirlineNameEn = rs.getString(14);
                source.flightAirlineName = rs.getString(15);
                source.flightIcaoCode = rs.getString(16);
                source.flightNumber = rs.getString(17);
                source.fltLegSeqNo = rs.getString(18);
                source.gateInfo = rs.getString(19);
                source.loungeInfo = rs.getString(20);
                source.schdArrOnlyDateLt = rs.getString(21);
                source.schdArrOnlyTimeLt = rs.getString(22);
                source.sourceData = rs.getString(23);
                source.statusInfo = rs.getString(24);
                source.terminalInfo = rs.getString(25);
                source.arrTerminalInfo = rs.getString(26);
                source.createdAt = rs.getLong(27);
                source.actDepDateTimeLt = rs.getString(28);
                source.schdDepOnlyDateLt = rs.getString(29);
                source.schdDepOnlyTimeLt = rs.getString(30);
                result.add(source);
            }
            if (!result.isEmpty()) {
                return result;
            }
            if (!result.isEmpty()) {
                return result;
            }
        }
        return Collections.emptyList();
    }
}
