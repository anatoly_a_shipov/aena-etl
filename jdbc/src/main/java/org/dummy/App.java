package org.dummy;

import static org.dummy.DateTimeUtils.seconds;

/**
 * ETL.
 */
public class App {

    public static void main(String[] args) {
        long begin = System.nanoTime();
        Etl.run();
        System.out.println("ETL took " + seconds(begin));
    }
}
