package org.dummy;

/**
 * If value is empty.
 */
public final class EmptinessUtils {

    /**
     * Constructor.
     */
    private EmptinessUtils() {
        //Utility
    }

    /**
     * Checks if a String is blank. A blank string is one that is {@code null}, empty, or when trimmed using
     * {@link String#trim()} is empty.
     * @param s the String to check, may be {@code null}
     * @return {@code true} if the String is {@code null}, empty, or trims to empty.
     */
    public static boolean isBlank(final String s) {
        return null == s || s.trim().isEmpty();
    }
}