package org.dummy;


import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import static org.dummy.EmptinessUtils.isBlank;

/**
 * Date and Time Utils.
 */
@SuppressWarnings("squid:S2259")
public final class DateTimeUtils {

    private static final String DATE_TIME_FORMAT = "dd/MM/yy HH:mm";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yy");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    private static final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();

    /**
     * Constructor.
     */
    private DateTimeUtils() {
        //Utility
    }

    /**
     * String to {@link LocalDate}.
     * @param d date string
     * @return {@link LocalDate} of date string
     */
    public static LocalDate toLocalDate(String d) {
        if (!isBlank(d)) {
            return LocalDate.parse(d, DATE_FORMATTER);
        }
        return null;
    }

    /**
     * String to {@link LocalTime}.
     * @param t time string
     * @return {@link LocalTime} of time string
     */
    public static LocalTime toLocalTime(String t) {
        if (!isBlank(t)) {
            return LocalTime.from(TIME_FORMATTER.parse(t));
        }
        return null;
    }

    /**
     * Date and Time strings to {@link LocalDateTime}
     * @param date date string
     * @param time time string
     * @return {@link LocalDateTime} of date and time string
     */
    public static LocalDateTime toLocalDateTime(String date, String time) {
        if (!isBlank(date) && !isBlank(time)) {
            return toLocalDate(date).atTime(toLocalTime(time));
        }
        return null;
    }

    /**
     * {@link Timestamp} of a {@link LocalDateTime}.
     * @param ldt {@link LocalDateTime}
     * @return {@link Timestamp}
     */
    public static Timestamp toTimestamp(LocalDateTime ldt) {
        if (null != ldt) {
            return Timestamp.from(ldt.atZone(DEFAULT_ZONE_ID).toInstant());
        }
        return null;
    }

    /**
     * Date and Time strings to {@link Timestamp}.
     * @param date date
     * @param time time
     * @return {@link Timestamp}
     */
    public static Timestamp toTimestamp(String date, String time) {
        return toTimestamp(toLocalDateTime(date, time));
    }

    /**
     * Date and Time string to LocalDateTime
     * @param dateTime Date and Time string
     * @return LocalDateTime of Date and Time String
     */
    public static LocalDateTime toLocalDateTime(String dateTime) {
        if (!isBlank(dateTime)) {
            return LocalDateTime.parse(dateTime, DATE_TIME_FORMATTER);
        } else {
            return null;
        }
    }

    /**
     * Seconds between two nanos.
     * @param begin begin
     * @param end end
     * @return time elapsed in seconds
     */
    @SuppressWarnings("java:S1905")
    public static double seconds(long begin, long end) {
        return ((double) (end - begin)) / 1_000_000_000.0;
    }

    /**
     * Seconds elapsed sinse begin.
     * @param begin begin
     * @return time elapsed in seconds
     */
    public static double seconds(long begin) {
        return seconds(begin, System.nanoTime());
    }

    /**
     * Current {@link Timestamp}.
     * @return {@link Timestamp}
     */
    public static Timestamp nowTimestamp() {
        return Timestamp.from(Instant.now());
    }
}
