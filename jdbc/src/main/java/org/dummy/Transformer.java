package org.dummy;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.StringJoiner;
import static org.dummy.DateTimeUtils.toLocalDateTime;
import static org.dummy.DateTimeUtils.toTimestamp;
import static org.dummy.EmptinessUtils.isBlank;

public class Transformer implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String EMPTY_STRING = "";
    private static final String DELIMITER_COMMA = ",";
    private static final String MINUS_SIGN = "-";

    final List<Source> sources;
    Long id = null;
    final Timestamp createdAt;

    // LIFO values
    final List<String> baggageInfos = new ArrayList<>(0);
    final List<String> counters = new ArrayList<>(0);
    final List<String> gateInfos = new ArrayList<>(0);
    final List<String> loungeInfos = new ArrayList<>(0);
    final List<String> terminalInfos = new ArrayList<>(0);
    final List<String> arrTerminalInfos = new ArrayList<>(0);
    // Latest values
    final List<String> depAptCodeIatas = new ArrayList<>(0);
    final List<String> arrAptCodeIatas = new ArrayList<>(0);
    final List<String> flightIcaoCodes = new ArrayList<>(0);
    final List<String> flightNumbers = new ArrayList<>(0);
    final List<String> carrierIcaoCodes = new ArrayList<>(0);
    final List<String> carrierNumbers = new ArrayList<>(0);
    final List<String> fltLegSeqNos = new ArrayList<>(0);
    final List<String> aircraftNameScheduleds = new ArrayList<>(0);
    final List<String> sourceDatas = new ArrayList<>(0);
    //
    final List<Timestamp> schdDepLts = new ArrayList<>(0);
    final List<Timestamp> schdArrLts = new ArrayList<>(0);
    final List<String> statusInfos = new ArrayList<>(0);
    final List<String> estDepDateTimeLts = new ArrayList<>(0);
    final List<String> estArrDateTimeLts = new ArrayList<>(0);
    final List<String> actDepDateTimeLts = new ArrayList<>(0);
    final List<String> actArrDateTimeLts = new ArrayList<>(0);

    public Transformer(List<Source> s, Timestamp c) {
        this.sources = s;
        this.createdAt = c;
    }

    public void transform() {
        for (Source sourceItem : sources) {
            // LIFO values
            putOnTheList(this.baggageInfos, sourceItem.baggageInfo);
            putOnTheList(this.counters, sourceItem.counter);
            putOnTheList(this.gateInfos, sourceItem.gateInfo);
            putOnTheList(this.loungeInfos, sourceItem.loungeInfo);
            putOnTheList(this.terminalInfos, sourceItem.terminalInfo);
            putOnTheList(this.arrTerminalInfos, sourceItem.arrTerminalInfo);
            // Latest values
            putOnTheList(this.depAptCodeIatas, sourceItem.depAptCodeIata);
            putOnTheList(this.arrAptCodeIatas, sourceItem.arrAptCodeIata);
            putOnTheList(this.flightIcaoCodes, sourceItem.flightIcaoCode);
            putOnTheList(this.flightNumbers, sourceItem.flightNumber);
            putOnTheList(this.carrierIcaoCodes, sourceItem.carrierIcaoCode);
            putOnTheList(this.carrierNumbers, sourceItem.carrierNumber);
            putOnTheList(this.fltLegSeqNos, sourceItem.fltLegSeqNo);
            putOnTheList(this.aircraftNameScheduleds, sourceItem.aircraftNameScheduled);
            putOnTheList(this.sourceDatas, sourceItem.sourceData);
            //
            putOnTheList(this.schdDepLts, toTimestamp(sourceItem.schdDepOnlyDateLt, sourceItem.schdDepOnlyTimeLt));
            putOnTheList(this.schdArrLts, toTimestamp(sourceItem.schdArrOnlyDateLt, sourceItem.schdArrOnlyTimeLt));
            putOnTheList(this.statusInfos, sourceItem.statusInfo);
            putOnTheList(this.estDepDateTimeLts, sourceItem.estDepDateTimeLt);
            putOnTheList(this.estArrDateTimeLts, sourceItem.estArrDateTimeLt);
            putOnTheList(this.actDepDateTimeLts, sourceItem.actDepDateTimeLt);
            putOnTheList(this.actArrDateTimeLts, sourceItem.actArrDateTimeLt);
        }
    }

    public static Long getId(Connection conn, String flightIcaoCode, String flightNumber, Timestamp schdDepLt) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(
                " SELECT id FROM destination_data " +
                        "  WHERE flight_code = ? " +
                        "  AND flight_number = ? " +
                        "  AND schd_dep_lt = ?")) {
            stmt.setString(1, flightIcaoCode);
            stmt.setString(2, flightNumber);
            stmt.setTimestamp(3, schdDepLt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getLong(1);
            }
            return null;
        }
    }

    public static int[] insert(Connection conn, List<Transformer> inserts) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO destination_data ( " +
                        " adep, ades, " +
                        " flight_code, flight_number, " +
                        " carrier_code, carrier_number, " +
                        " status_info, " +
                        " schd_dep_lt, schd_arr_lt, " +
                        " est_dep_lt, est_arr_lt, " +
                        " act_dep_lt, act_arr_lt, " +
                        " flt_leg_seq_no, " +
                        " aircraft_name_scheduled, " +
                        " baggage_info, " +
                        " counter, " +
                        " gate_info, " +
                        " lounge_info, " +
                        " terminal_info, " +
                        " arr_terminal_info, " +
                        " source_data, " +
                        " created_at " +
                        ") " +
                        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")) {
            for (Transformer insert : inserts) {
                upsert(stmt, insert);
                stmt.addBatch();
            }
            return stmt.executeBatch();
        }
    }

    public static int[] update(Connection conn, List<Transformer> updates) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("UPDATE destination_data  " +
                " SET adep = ?, ades = ?, " +
                " flight_code = ?, flight_number = ?, " +
                " carrier_code = ?, carrier_number = ?, " +
                " status_info = ?, " +
                " schd_dep_lt = ?, schd_arr_lt = ?, " +
                " est_dep_lt = ?, est_arr_lt = ?, " +
                " act_dep_lt = ?, act_arr_lt = ?, " +
                " flt_leg_seq_no = ?, " +
                " aircraft_name_scheduled = ?, " +
                " baggage_info = ?, " +
                " counter = ?, " +
                " gate_info = ?, " +
                " lounge_info = ?, " +
                " terminal_info = ?, " +
                " arr_terminal_info = ?, " +
                " source_data = ?, " +
                " created_at = ? " +
                " WHERE id = ?;")) {
            for (Transformer update : updates) {
                upsert(stmt, update);
                stmt.setLong(24, update.id);
                stmt.addBatch();
            }
            return stmt.executeBatch();
        }
    }

    private static void upsert(PreparedStatement stmt, Transformer transformer) throws SQLException {
        stmt.setString(1, getLastElement(transformer.depAptCodeIatas));
        stmt.setString(2, getLastElement(transformer.arrAptCodeIatas));
        stmt.setString(3, getLastElement(transformer.flightIcaoCodes));
        stmt.setString(4, getLastElement(transformer.flightNumbers));
        stmt.setString(5, getLastElement(transformer.carrierIcaoCodes));
        stmt.setString(6, getLastElement(transformer.carrierNumbers));
        stmt.setString(7, getLastElement(transformer.statusInfos));
        stmt.setTimestamp(8, latestTimestamp(transformer.schdDepLts));
        stmt.setTimestamp(9, latestTimestamp(transformer.schdArrLts));
        stmt.setTimestamp(10, toTimestamp(toLocalDateTime(getLastElement(transformer.estDepDateTimeLts))));
        stmt.setTimestamp(11, toTimestamp(toLocalDateTime(getLastElement(transformer.estArrDateTimeLts))));
        stmt.setTimestamp(12, toTimestamp(toLocalDateTime(getLastElement(transformer.actDepDateTimeLts))));
        stmt.setTimestamp(13, toTimestamp(toLocalDateTime(getLastElement(transformer.actArrDateTimeLts))));
        stmt.setInt(14, Integer.valueOf(getLastElement(transformer.fltLegSeqNos)));
        stmt.setString(15, getLastElement(transformer.aircraftNameScheduleds));
        stmt.setString(16, reverseJoin(transformer.baggageInfos));
        stmt.setString(17, reverseJoin(transformer.counters));
        stmt.setString(18, reverseJoin(transformer.gateInfos));
        stmt.setString(19, reverseJoin(transformer.loungeInfos));
        stmt.setString(20, reverseJoin(transformer.terminalInfos));
        stmt.setString(21, reverseJoin(transformer.arrTerminalInfos));
        stmt.setString(22, getLastElement(transformer.sourceDatas));
        stmt.setTimestamp(23, transformer.createdAt);
    }

    static String reverseJoin(List<String> list) {
        if (!list.isEmpty()) {
            StringJoiner joiner = new StringJoiner(DELIMITER_COMMA);
            ListIterator<String> it = list.listIterator(list.size());
            String s;
            while (it.hasPrevious()) {
                s = it.previous();
                if (!isBlank(s)) {
                    joiner.add(s);
                }
            }
            return joiner.toString();
        }
        return EMPTY_STRING;
    }

    static String getLastElement(List<String> l) {
        if (null != l && !l.isEmpty()) {
            return l.get(l.size() - 1);
        }
        return EMPTY_STRING;
    }

    static Timestamp latestTimestamp(List<Timestamp> l) {
        if (null != l && !l.isEmpty()) {
            return l.get(l.size() - 1);
        }
        return null;
    }

    static void putOnTheList(List<String> list, String s) {
        if ((null != list && !isBlank(s) && !MINUS_SIGN.equals(s)) && (list.isEmpty() || !list.get(list.size() - 1).equals(s))) {
            list.add(s);
        }
    }

    static void putOnTheList(List<Timestamp> list, Timestamp t) {
        if ((null != list && null != t) && (list.isEmpty() || !list.get(list.size() - 1).equals(t))) {
            list.add(t);
        }
    }
}
