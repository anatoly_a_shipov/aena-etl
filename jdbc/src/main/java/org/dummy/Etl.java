package org.dummy;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static org.dummy.DateTimeUtils.*;

/**
 * ETL.
 */
public final class Etl {

    private static final String JDBC = "jdbc:postgresql://localhost:5432/postgres?user=postgres&password=postgres&ssl=false";
    private static final int CHUNK_SIZE = 100;

    /**
     * Constructor.
     */
    private Etl() {
        //
    }

    private static Connection connection() throws SQLException {
        return DriverManager.getConnection(JDBC);
    }

    @SuppressWarnings("java:S3776")
    public static void run() {
        try (Connection conn = connection()) {
            long begin = System.nanoTime();
            List<Source> flights = Source.flights(conn);
            System.out.println("Flights query took " + seconds(begin));
            List<List<Source>> partitions = chunkBy(flights);
            Timestamp createdAt = nowTimestamp();
            for (int i = 0; i < partitions.size(); i++) {
                List<Source> partition = partitions.get(i);
                begin = System.nanoTime();
                List<Transformer> inserts = new ArrayList<>(0);
                List<Transformer> updates = new ArrayList<>(0);
                for (Source flight : partition) {
                    Timestamp schdDepLt = toTimestamp(flight.schdDepOnlyDateLt, flight.schdDepOnlyTimeLt);
                    List<Source> sources = Source.sources(conn, flight);
                    sources.size();
                    if (!sources.isEmpty() && null != schdDepLt) {
                        Transformer transformer = new Transformer(sources, createdAt);
                        transformer.transform();
                        Long id = Transformer.getId(conn, flight.flightIcaoCode, flight.flightNumber, schdDepLt);
                        if (null == id) {
                            inserts.add(transformer);
                        } else {
                            transformer.id = id;
                            updates.add(transformer);
                        }
                    }
                }
                if (!inserts.isEmpty()) {
                    Transformer.insert(conn, inserts);
                }
                if (!updates.isEmpty()) {
                    Transformer.update(conn, updates);
                }
                System.out.println("Partition " + i + " took " + seconds(begin));
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    static <T> List<List<T>> chunkBy(List<T> l) {
        List<List<T>> partitions = new ArrayList<>();
        for (int i = 0; i < l.size(); i += CHUNK_SIZE) {
            partitions.add(l.subList(i, Math.min(i + CHUNK_SIZE, l.size())));
        }
        return partitions;
    }
}
