import { parseDateTime, putString, putTime } from './util.js';

export class Transformer {
    constructor(
        sources,
        baggageInfos,
        counters,
        gateInfos,
        loungeInfos,
        terminalInfos,
        arrTerminalInfos,
        depAptCodeIatas,
        arrAptCodeIatas,
        flightIcaoCodes,
        flightNumbers,
        carrierIcaoCodes,
        carrierNumbers,
        fltLegSeqNos,
        aircraftNameScheduleds,
        sourceDatas,
        schdDepLts,
        schdArrLts,
        statusInfos,
        estDepDateTimeLts,
        estArrDateTimeLts,
        actDepDateTimeLts,
        actArrDateTimeLts) {
        this.sources = sources;
        // LIFO values
        this.baggageInfos = baggageInfos;
        this.counters = counters;
        this.gateInfos = gateInfos;
        this.loungeInfos = loungeInfos;
        this.terminalInfos = terminalInfos;
        this.arrTerminalInfos = arrTerminalInfos;
        // Latest values
        this.depAptCodeIatas = depAptCodeIatas;
        this.arrAptCodeIatas = arrAptCodeIatas;
        this.flightIcaoCodes = flightIcaoCodes;
        this.flightNumbers = flightNumbers;
        this.carrierIcaoCodes = carrierIcaoCodes;
        this.carrierNumbers = carrierNumbers;
        this.fltLegSeqNos = fltLegSeqNos;
        this.aircraftNameScheduleds = aircraftNameScheduleds;
        this.sourceDatas = sourceDatas;
        this.schdDepLts = schdDepLts;
        this.schdArrLts = schdArrLts;
        this.statusInfos = statusInfos;
        this.estDepDateTimeLts = estDepDateTimeLts;
        this.estArrDateTimeLts = estArrDateTimeLts;
        this.actDepDateTimeLts = actDepDateTimeLts;
        this.actArrDateTimeLts = actArrDateTimeLts;
    }
    static build(sources) {
        return new Transformer(sources, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [],);
    };
    transform() {
        let source;
        let transformer = this;
        for (let i = 0; i < this.sources.length; i++) {
            source = this.sources[i];
            // LIFO values
            putString(transformer.baggageInfos, source.baggage_info);
            putString(transformer.counters, source.counter);
            putString(transformer.gateInfos, source.gate_info);
            putString(transformer.loungeInfos, source.lounge_info);
            putString(transformer.terminalInfos, source.terminal_info);
            putString(transformer.arrTerminalInfos, source.arr_terminal_info);
            // Latest values
            putString(transformer.depAptCodeIatas, source.dep_apt_code_iata);
            putString(transformer.arrAptCodeIatas, source.arr_apt_code_iata);
            putString(transformer.flightIcaoCodes, source.flight_icao_code);
            putString(transformer.flightNumbers, source.flight_number);
            putString(transformer.carrierIcaoCodes, source.carrier_icao_code);
            putString(transformer.carrierNumbers, source.carrier_number);
            putString(transformer.fltLegSeqNos, source.flt_leg_seq_no);
            putString(transformer.aircraftNameScheduleds, source.aircraft_name_scheduled);
            putString(transformer.sourceDatas, source.source_data);
            putTime(transformer.schdDepLts, parseDateTime(source.schd_dep_only_date_lt, source.schd_dep_only_time_lt));
            putTime(transformer.schdArrLts, parseDateTime(source.schd_arr_only_date_lt, source.schd_arr_only_time_lt));
            putString(transformer.statusInfos, source.status_info);
            putString(transformer.estDepDateTimeLts, source.est_dep_date_time_lt);
            putString(transformer.estArrDateTimeLts, source.est_arr_date_time_lt);
            putString(transformer.actDepDateTimeLts, source.act_dep_date_time_lt);
            putString(transformer.actArrDateTimeLts, source.act_arr_date_time_lt);
        }
    }
};
