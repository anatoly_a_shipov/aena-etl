export const createdAt = new Date();

const minusSign = '-';

export const parseDateTime = (dateStr, timeStr) => {
    if (dateStr && timeStr) {
        // dd/mm/yy
        let dateParts = dateStr.split('/');
        let dd = dateParts[0];
        let mm = dateParts[1];
        let yyyy = '20' + dateParts[2];
        // hh:ss
        let timeParts = timeStr.split(':');
        let h = timeParts[0];
        let m = timeParts[1];
        let dateTimeStr = yyyy + '-' + mm + '-' + dd + 'T' + h + ':' + m + ':00.000Z';
        let dateTime = new Date(dateTimeStr);
        if (isNaN(dateTime.getTime())) {
            //console.error(`Can not parse date ${dateStr} and ${timeStr}`);
            return null;
        }
        //console.log(dateParts, timeParts, dateTime);
        return dateTime;
    }
    return null;
};

const chunkSize = 100;
// https://stackoverflow.com/a/24782004
export const chunk = (arr) => {
    if (chunkSize <= 0) throw 'Invalid chunk size';
    let chunks = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
        chunks.push(arr.slice(i, i + chunkSize));
    }
    return chunks;
};

export const putString = (a, s) => {
    if (a && Array.isArray(a) && s && (typeof s === 'string' || s instanceof String) && s.length > 0 && !(minusSign === s)) {
        let arrayLength = a.length;
        if (0 === arrayLength || !(a[arrayLength - 1] === s)) {
            a.push(s);
        }
    }
    return a;
};

export const putTime = (a, t) => {
    if (a && Array.isArray(a) && t && t instanceof Date) {
        let arrayLength = a.length;
        if (0 === arrayLength || !(a[arrayLength - 1].getTime() === t.getTime())) {
            a.push(t);
        }
    }
    return a;
};

export const reverseJoin = (a) => {
    if (a && Array.isArray(a)) {
        return [...a].reverse().join(',');
    }
    return '';
};

export const lastStringElement = (a) => {
    if (a && Array.isArray(a)) {
        let arrayLength = a.length;
        if (0 < arrayLength) {
            return a[arrayLength - 1];
        }
    }
    return '';
};

export const lastTimeElement = (a) => {
    if (a && Array.isArray(a)) {
        let arrayLength = a.length;
        if (0 < arrayLength) {
            return a[arrayLength - 1];
        }
    }
    return null;
};
