
export const flightsSql = `SELECT DISTINCT 
flight_icao_code,
flight_number,
schd_dep_only_date_lt,
schd_dep_only_time_lt
FROM aenaflight_2017_01
WHERE schd_dep_only_date_lt IS NOT NULL AND schd_dep_only_date_lt <> ''
AND schd_dep_only_time_lt IS NOT NULL AND schd_dep_only_time_lt <> ''`;

export const sourcesSql = `SELECT
id,
act_arr_date_time_lt,
aircraft_name_scheduled,
arr_apt_name_es,
arr_apt_code_iata,
baggage_info,
coalesce(carrier_airline_name_en, '') AS carrier_airline_name_en,
coalesce(carrier_icao_code, '') AS carrier_icao_code,
coalesce(carrier_number, '') AS carrier_number,
counter,
dep_apt_name_es,
dep_apt_code_iata,
est_arr_date_time_lt,
est_dep_date_time_lt,
coalesce(flight_airline_name_en, '') AS flight_airline_name_en,
flight_airline_name,
flight_icao_code,
flight_number,
flt_leg_seq_no,
gate_info,
lounge_info,
schd_arr_only_date_lt,
schd_arr_only_time_lt,
source_data,
status_info,
terminal_info,
arr_terminal_info,
created_at,
act_dep_date_time_lt,
schd_dep_only_date_lt,
schd_dep_only_time_lt 
FROM aenaflight_2017_01 
WHERE 
flight_icao_code = $1
AND flight_number = $2
AND schd_dep_only_date_lt = $3
AND schd_dep_only_time_lt = $4
ORDER BY created_at ASC`;

export const targetIdSql = `SELECT id FROM destination_data WHERE flight_code = $1 AND flight_number = $2 AND schd_dep_lt = $3`;

export const insertSQL = `INSERT INTO destination_data (
    adep, ades,
    flight_code, flight_number,
    carrier_code, carrier_number,
    status_info,
    schd_dep_lt, schd_arr_lt,
    est_dep_lt, est_arr_lt,
    act_dep_lt,	act_arr_lt,
    flt_leg_seq_no,
    aircraft_name_scheduled,
    baggage_info,
    counter,
    gate_info,
    lounge_info,
    terminal_info,	
    arr_terminal_info,
    source_data,
    created_at
  )
VALUES (
    $1, $2, $3, $4,	$5,	$6,	$7, $8, $9, $10,
    $11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
    $21, $22, $23
    );`;

export const updateSQL = `UPDATE destination_data 
    SET adep = $2, ades = $3,
    flight_code = $4, flight_number = $5,
    carrier_code = $6, carrier_number = $7,
    status_info = $8,
    schd_dep_lt = $9, schd_arr_lt = $10,
    est_dep_lt = $11, est_arr_lt = $12,
    act_dep_lt = $13,	act_arr_lt = $14,
    flt_leg_seq_no = $15,
    aircraft_name_scheduled = $16,
    baggage_info = $17,
    counter = $18,
    gate_info = $19,
    lounge_info = $20,
    terminal_info = $21,	
    arr_terminal_info = $22,
    source_data = $23,
    created_at = $24
    WHERE id = $1;`;