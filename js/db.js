import pkgpg from 'pg';
const { Client } = pkgpg;

export const client = new Client({
    user: 'postgres',
    database: 'postgres',
    password: 'postgres',
    port: 5432,
    host: 'localhost',
    // CockroachDB - uncomment lines below
    /*    
    ssl: {
        rejectUnauthorized: false
        
    }*/
});
client.connect();
