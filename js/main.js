import { client } from './db.js';
import { chunk, parseDateTime, createdAt, lastStringElement, lastTimeElement, reverseJoin } from './util.js';
import { sourcesSql, flightsSql, targetIdSql, insertSQL, updateSQL } from './sql.js';
import { Transformer } from './target.js';

const timingLbl = 'ETL took';
const etl = async () => {
    console.time(timingLbl);
    let flightsRes = await client.query(flightsSql);
    const partitions = chunk(flightsRes.rows);
    for (let partition of partitions) {
        for (let flight of partition) {
            let sourcesRes = await client.query(sourcesSql, [flight.flight_icao_code, flight.flight_number, flight.schd_dep_only_date_lt, flight.schd_dep_only_time_lt]);
            const transformer = Transformer.build(sourcesRes.rows);
            transformer.transform();
            const targetIdSqlParams = [flight.flight_icao_code, flight.flight_number, parseDateTime(flight.schd_dep_only_date_lt, flight.schd_dep_only_time_lt)];
            let persistedTargetRes = await client.query(targetIdSql, targetIdSqlParams);
            if (persistedTargetRes.rowCount === 0) {
                let insertRes = await client.query(insertSQL, [
                    lastStringElement(transformer.depAptCodeIatas), lastStringElement(transformer.arrAptCodeIatas),
                    lastStringElement(transformer.flightIcaoCodes), lastStringElement(transformer.flightNumbers),
                    lastStringElement(transformer.carrierIcaoCodes), lastStringElement(transformer.carrierNumbers),
                    lastStringElement(transformer.statusInfos),
                    lastTimeElement(transformer.schdDepLts), lastTimeElement(transformer.schdArrLts),
                    parseDateTime(lastStringElement(transformer.estDepDateTimeLts)), parseDateTime(lastStringElement(transformer.estArrDateTimeLts)),
                    parseDateTime(lastStringElement(transformer.actDepDateTimeLts)), parseDateTime(lastStringElement(transformer.actArrDateTimeLts)),
                    Number(lastStringElement(transformer.fltLegSeqNos)),
                    lastStringElement(transformer.aircraftNameScheduleds),
                    reverseJoin(transformer.baggageInfos),
                    reverseJoin(transformer.counters),
                    reverseJoin(transformer.gateInfos),
                    reverseJoin(transformer.loungeInfos),
                    reverseJoin(transformer.terminalInfos),
                    reverseJoin(transformer.arrTerminalInfos),
                    lastStringElement(transformer.sourceDatas),
                    createdAt]);
            } else if (persistedTargetRes.rowCount > 1) {
                console.log(`Multiple targets for ${targetIdSqlParams}`);
            } else {
                let updateRes = await client.query(updateSQL, [
                    persistedTargetRes.rows[0].id,
                    lastStringElement(transformer.depAptCodeIatas), lastStringElement(transformer.arrAptCodeIatas),
                    lastStringElement(transformer.flightIcaoCodes), lastStringElement(transformer.flightNumbers),
                    lastStringElement(transformer.carrierIcaoCodes), lastStringElement(transformer.carrierNumbers),
                    lastStringElement(transformer.statusInfos),
                    lastTimeElement(transformer.schdDepLts), lastTimeElement(transformer.schdArrLts),
                    parseDateTime(lastStringElement(transformer.estDepDateTimeLts)), parseDateTime(lastStringElement(transformer.estArrDateTimeLts)),
                    parseDateTime(lastStringElement(transformer.actDepDateTimeLts)), parseDateTime(lastStringElement(transformer.actArrDateTimeLts)),
                    Number(lastStringElement(transformer.fltLegSeqNos)),
                    lastStringElement(transformer.aircraftNameScheduleds),
                    reverseJoin(transformer.baggageInfos),
                    reverseJoin(transformer.counters),
                    reverseJoin(transformer.gateInfos),
                    reverseJoin(transformer.loungeInfos),
                    reverseJoin(transformer.terminalInfos),
                    reverseJoin(transformer.arrTerminalInfos),
                    lastStringElement(transformer.sourceDatas),
                    createdAt
                ]);
            };
        };
    };
    console.timeEnd(timingLbl);
    return;
};

etl().then(res => process.exit(0))
    .catch(err => {
        console.log(err);
        process.exit(1);
    });
