#!/bin/bash

set -e

DUMMY_UID=10001

docker-compose down
docker-compose -f docker-compose-01-postgres-init.yml down
docker-compose -f docker-compose-02-untar-and-chown.yml down
docker-compose -f docker-compose-03-restore-dump.yml down

sudo -S <<<"${PASSWORD}" chown -R ${DUMMY_UID}:${DUMMY_UID} ${HOME}/docker-volumes/aena/
rm -rf ${HOME}/docker-volumes/aena/postgres/
mkdir -p ${HOME}/docker-volumes/aena/postgres/

docker-compose -f docker-compose-01-postgres-init.yml up -d 
sleep 10s 
docker-compose -f docker-compose-01-postgres-init.yml down

docker-compose -f docker-compose-02-untar-and-chown.yml up 
docker-compose -f docker-compose-02-untar-and-chown.yml down

docker-compose -f docker-compose-03-restore-dump.yml up -d 
sleep 10s 
docker exec aena-postgres-03 /dump/restore-dump.bash 
docker-compose -f docker-compose-03-restore-dump.yml down

docker-compose up -d
