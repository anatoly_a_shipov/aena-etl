#!/bin/bash

psql -U ${POSTGRES} -d ${POSTGRES} -f /dump/${DUMP_FILE_NAME}
rm -rf /dump/${DUMP_FILE_NAME}
psql -U ${POSTGRES} -d ${POSTGRES} -a -f /dump/destination-data-and-indexes-drop.sql
psql -U ${POSTGRES} -d ${POSTGRES} -a -f /dump/destination-data-and-indexes-create.sql
