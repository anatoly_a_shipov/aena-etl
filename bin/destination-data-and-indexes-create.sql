CREATE SEQUENCE IF NOT EXISTS destination_data_id_seq;

CREATE TABLE IF NOT EXISTS destination_data
(
  id BIGINT PRIMARY KEY DEFAULT nextval('destination_data_id_seq'),
  adep CHARACTER VARYING(8),
  ades CHARACTER VARYING(8),
  flight_code CHARACTER VARYING(8),
  flight_number CHARACTER VARYING(8),
  carrier_code CHARACTER VARYING(8),
  carrier_number CHARACTER VARYING(8),
  status_info CHARACTER VARYING(256),
  schd_dep_lt TIMESTAMP WITHOUT TIME ZONE,
  schd_arr_lt TIMESTAMP WITHOUT TIME ZONE,
  est_dep_lt TIMESTAMP WITHOUT TIME ZONE,
  est_arr_lt TIMESTAMP WITHOUT TIME ZONE,
  act_dep_lt TIMESTAMP WITHOUT TIME ZONE,
  act_arr_lt TIMESTAMP WITHOUT TIME ZONE,
  flt_leg_seq_no INTEGER,
  aircraft_name_scheduled text,
  baggage_info CHARACTER VARYING(128),
  counter CHARACTER VARYING(128),
  gate_info CHARACTER VARYING(128),
  lounge_info CHARACTER VARYING(128),
  terminal_info CHARACTER VARYING(128),
  arr_terminal_info CHARACTER VARYING(128),
  source_data TEXT,
  created_at TIMESTAMP WITHOUT TIME ZONE
);

CREATE INDEX IF NOT EXISTS aenaflight_2017_01_search_index ON aenaflight_2017_01 (flight_icao_code, flight_number, schd_dep_only_date_lt, schd_dep_only_time_lt);

CREATE INDEX IF NOT EXISTS destination_data_search_index ON destination_data (flight_code, flight_number, schd_dep_lt);
