DROP INDEX IF EXISTS aenaflight_2017_01_search_index;

DROP INDEX IF EXISTS destination_data_search_index;

DROP TABLE IF EXISTS destination_data;

DROP SEQUENCE IF exists destination_data_id_seq;
