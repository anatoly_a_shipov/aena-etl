#!/bin/bash

set -e

rm -rf /dump/${DUMP_FILE_NAME} 
tar -xzf /dump/aena_test_data.tar.gz -C /dump/
chown -R ${DUMMY_UID}:${DUMMY_UID} /var/lib/postgresql/data/ 
chown -R ${DUMMY_UID}:${DUMMY_UID} /dump/
