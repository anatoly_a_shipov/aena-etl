package main

import (
	"context"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	dbConnectionString = "postgres://postgres:postgres@localhost:5432/postgres"
)

func checkError(err error) {
	if nil != err {
		log.Fatal(err)
	}
}

func main() {
	mainBegin := time.Now()
	ctx := context.Background()
	pool, err := pgxpool.Connect(ctx, dbConnectionString)
	checkError(err)

	flights := Flights(ctx, pool)
	log.Printf("Flights count %d", len(flights))

	flightChunks := ChunkBy(flights)

	createdAt := time.Now()
	for chunkNo, flightChunk := range flightChunks {
		begin := time.Now()
		Etl(ctx, pool, flightChunk, createdAt)
		log.Printf("Chunk %d took %s", chunkNo, time.Since(begin))
	}
	log.Printf("ETL took %s", time.Since(mainBegin))
}
