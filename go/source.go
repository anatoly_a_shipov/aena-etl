package main

import (
	"context"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Source struct {
	actArrDateTimeLt      string
	aircraftNameScheduled string
	arrAptNameEs          string
	arrAptCodeIata        string
	baggageInfo           string
	carrierAirlineNameEn  string
	carrierIcaoCode       string
	carrierNumber         string
	counter               string
	depAptNameEs          string
	depAptCodeIata        string
	estArrDateTimeLt      string
	estDepDateTimeLt      string
	flightAirlineNameEn   string
	flightAirlineName     string
	flightIcaoCode        string
	flightNumber          string
	fltLegSeqNo           string
	gateInfo              string
	loungeInfo            string
	schdArrOnlyDateLt     string
	schdArrOnlyTimeLt     string
	sourceData            string
	statusInfo            string
	terminalInfo          string
	arrTerminalInfo       string
	createdAt             uint32
	actDepDateTimeLt      string
	schdDepOnlyDateLt     string
	schdDepOnlyTimeLt     string
}

const (
	chunkSize   = 100
	sourceQuery = `SELECT
act_arr_date_time_lt,
aircraft_name_scheduled,
arr_apt_name_es,
arr_apt_code_iata,
baggage_info,
coalesce(carrier_airline_name_en, '') AS carrier_airline_name_en,
coalesce(carrier_icao_code, '') AS carrier_icao_code,
coalesce(carrier_number, '') AS carrier_number,
counter,
dep_apt_name_es,
dep_apt_code_iata,
est_arr_date_time_lt,
est_dep_date_time_lt,
coalesce(flight_airline_name_en, '') AS flight_airline_name_en,
flight_airline_name,
flight_icao_code,
flight_number,
flt_leg_seq_no,
gate_info,
lounge_info,
schd_arr_only_date_lt,
schd_arr_only_time_lt,
source_data,
status_info,
terminal_info,
arr_terminal_info,
created_at,
act_dep_date_time_lt,
schd_dep_only_date_lt,
schd_dep_only_time_lt 
FROM aenaflight_2017_01 
WHERE 
flight_icao_code = $1
AND flight_number = $2
AND schd_dep_only_date_lt = $3
AND schd_dep_only_time_lt = $4
ORDER BY created_at ASC`
	flightsQuery = `SELECT DISTINCT 
		flight_icao_code,
		flight_number,
		schd_dep_only_date_lt,
		schd_dep_only_time_lt
		FROM aenaflight_2017_01
		WHERE schd_dep_only_date_lt IS NOT NULL AND schd_dep_only_date_lt <> ''
		AND schd_dep_only_time_lt IS NOT NULL AND schd_dep_only_time_lt <> ''`
)

func Sources(ctx context.Context, pool *pgxpool.Pool, flight Source) []Source {
	rows, err := pool.Query(ctx, sourceQuery, flight.flightIcaoCode, flight.flightNumber, flight.schdDepOnlyDateLt, flight.schdDepOnlyTimeLt)
	checkError(err)
	sources := []Source{}
	source := Source{}
	for rows.Next() {
		source = Source{}
		err := rows.Scan(&source.actArrDateTimeLt,
			&source.aircraftNameScheduled,
			&source.arrAptNameEs,
			&source.arrAptCodeIata,
			&source.baggageInfo,
			&source.carrierAirlineNameEn,
			&source.carrierIcaoCode,
			&source.carrierNumber,
			&source.counter,
			&source.depAptNameEs,
			&source.depAptCodeIata,
			&source.estArrDateTimeLt,
			&source.estDepDateTimeLt,
			&source.flightAirlineNameEn,
			&source.flightAirlineName,
			&source.flightIcaoCode,
			&source.flightNumber,
			&source.fltLegSeqNo,
			&source.gateInfo,
			&source.loungeInfo,
			&source.schdArrOnlyDateLt,
			&source.schdArrOnlyTimeLt,
			&source.sourceData,
			&source.statusInfo,
			&source.terminalInfo,
			&source.arrTerminalInfo,
			&source.createdAt,
			&source.actDepDateTimeLt,
			&source.schdDepOnlyDateLt,
			&source.schdDepOnlyTimeLt,
		)
		checkError(err)
		sources = append(sources, source)
	}
	return sources
}

func Flights(ctx context.Context, pool *pgxpool.Pool) []Source {
	begin := time.Now()
	rows, err := pool.Query(ctx, flightsQuery)
	log.Printf("Flights query took %s", time.Since(begin))
	checkError(err)
	defer rows.Close()

	flights := []Source{}
	flight := Source{}
	begin = time.Now()
	for rows.Next() {
		flight = Source{}
		err := rows.Scan(
			&flight.flightIcaoCode,
			&flight.flightNumber,
			&flight.schdDepOnlyDateLt,
			&flight.schdDepOnlyTimeLt)
		checkError(err)
		flights = append(flights, flight)
	}
	log.Printf("Flight objects creation took %s", time.Since(begin))
	return flights
}

func ChunkBy(flights []Source) [][]Source {
	if 0 >= chunkSize {
		log.Panic("Can not chunk by 0 or negative integer")
	}
	flightsLen := len(flights)
	flightChunks := make([][]Source, (flightsLen+chunkSize-1)/chunkSize)
	upperLimit := 0
	for i := 0; i < flightsLen; i += chunkSize {
		upperLimit = i + chunkSize
		if upperLimit >= flightsLen {
			upperLimit = flightsLen
		}
		flightChunks[i/chunkSize] = flights[i:upperLimit]
	}
	return flightChunks
}
