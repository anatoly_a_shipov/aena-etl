package main

import (
	"context"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	selectSQL = `SELECT 
	id
  FROM destination_data
  WHERE flight_code = $1
  AND flight_number = $2
  AND schd_dep_lt = $3`

	// https://stackoverflow.com/a/40939364
	dateTimeLayout = "02/01/06 15:04"
	noRowsInRS     = "no rows in result set"
	minusSign      = "-"
	emptyString    = ""
	space          = " "
	comma          = ","

	insertSQL = `INSERT INTO destination_data (
		adep, ades,
		flight_code, flight_number,
		carrier_code, carrier_number,
		status_info,
		schd_dep_lt, schd_arr_lt,
		est_dep_lt, est_arr_lt,
		act_dep_lt,	act_arr_lt,
		flt_leg_seq_no,
		aircraft_name_scheduled,
		baggage_info,
		counter,
		gate_info,
		lounge_info,
		terminal_info,	
		arr_terminal_info,
		source_data,
		created_at
	  )
	VALUES (
		$1, $2, $3, $4,	$5,	$6,	$7, $8, $9, $10,
		$11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
		$21, $22, $23
		);`

	updateSQL = `UPDATE destination_data 
			SET adep = $2, ades = $3,
			flight_code = $4, flight_number = $5,
			carrier_code = $6, carrier_number = $7,
			status_info = $8,
			schd_dep_lt = $9, schd_arr_lt = $10,
			est_dep_lt = $11, est_arr_lt = $12,
			act_dep_lt = $13,	act_arr_lt = $14,
			flt_leg_seq_no = $15,
			aircraft_name_scheduled = $16,
			baggage_info = $17,
			counter = $18,
			gate_info = $19,
			lounge_info = $20,
			terminal_info = $21,	
			arr_terminal_info = $22,
			source_data = $23,
			created_at = $24
			WHERE id = $1;`
)

var (
	zeroDate = time.Time{}
)

func targetId(ctx context.Context, pool *pgxpool.Pool, flightIcaoCode string, flightNumber string, dt time.Time) (uint32, error) {
	var targetId uint32
	row := pool.QueryRow(ctx, selectSQL, flightIcaoCode, flightNumber, dt)
	err := row.Scan(&targetId)
	return targetId, err
}

type Transformer struct {
	// LIFO values
	baggageInfos     []string
	counters         []string
	gateInfos        []string
	loungeInfos      []string
	terminalInfos    []string
	arrTerminalInfos []string
	// Latest values
	depAptCodeIatas        []string
	arrAptCodeIatas        []string
	flightIcaoCodes        []string
	flightNumbers          []string
	carrierIcaoCodes       []string
	carrierNumbers         []string
	fltLegSeqNos           []string
	fltLegSeqNo            uint
	aircraftNameScheduleds []string
	sourceDatas            []string
	schdDepLts             []time.Time
	schdArrLts             []time.Time
	statusInfos            []string
	estDepDateTimeLts      []string
	estArrDateTimeLts      []string
	actDepDateTimeLts      []string
	actArrDateTimeLts      []string
	estDepLt               time.Time
	estArrLt               time.Time
	actDepLt               time.Time
	actArrLt               time.Time
}

func transform(sources []Source) Transformer {
	transformer := Transformer{}
	for _, source := range sources {
		// LIFO values
		transformer.baggageInfos = putString(transformer.baggageInfos, source.baggageInfo)
		transformer.counters = putString(transformer.counters, source.counter)
		transformer.gateInfos = putString(transformer.gateInfos, source.gateInfo)
		transformer.loungeInfos = putString(transformer.loungeInfos, source.loungeInfo)
		transformer.terminalInfos = putString(transformer.terminalInfos, source.terminalInfo)
		transformer.arrTerminalInfos = putString(transformer.arrTerminalInfos, source.arrTerminalInfo)
		// Latest values
		transformer.depAptCodeIatas = putString(transformer.depAptCodeIatas, source.depAptCodeIata)
		transformer.arrAptCodeIatas = putString(transformer.arrAptCodeIatas, source.arrAptCodeIata)
		transformer.flightIcaoCodes = putString(transformer.flightIcaoCodes, source.flightIcaoCode)
		transformer.flightNumbers = putString(transformer.flightNumbers, source.flightNumber)
		transformer.carrierIcaoCodes = putString(transformer.carrierIcaoCodes, source.carrierIcaoCode)
		transformer.carrierNumbers = putString(transformer.carrierNumbers, source.carrierNumber)
		transformer.fltLegSeqNos = putString(transformer.fltLegSeqNos, source.fltLegSeqNo)
		transformer.aircraftNameScheduleds = putString(transformer.aircraftNameScheduleds, source.aircraftNameScheduled)
		transformer.sourceDatas = putString(transformer.sourceDatas, source.sourceData)

		t, err := parseDateAndTime(source.schdDepOnlyDateLt, source.schdDepOnlyTimeLt)
		if nil == err {
			transformer.schdDepLts = putTime(transformer.schdDepLts, t)
		}

		t, err = parseDateAndTime(source.schdArrOnlyDateLt, source.schdArrOnlyTimeLt)
		if nil == err {
			transformer.schdArrLts = putTime(transformer.schdArrLts, t)
		}

		transformer.statusInfos = putString(transformer.statusInfos, source.statusInfo)
		transformer.estDepDateTimeLts = putString(transformer.estDepDateTimeLts, source.estDepDateTimeLt)
		transformer.estArrDateTimeLts = putString(transformer.estArrDateTimeLts, source.estArrDateTimeLt)
		transformer.actDepDateTimeLts = putString(transformer.actDepDateTimeLts, source.actDepDateTimeLt)
		transformer.actArrDateTimeLts = putString(transformer.actArrDateTimeLts, source.actArrDateTimeLt)
	}
	var err error

	fltLegSeqNo, err := strconv.Atoi(lastStringElement(transformer.fltLegSeqNos))
	checkError(err)
	transformer.fltLegSeqNo = uint(fltLegSeqNo)

	transformer.estDepLt, err = parseDateTime(lastStringElement(transformer.estDepDateTimeLts))
	checkError(err)

	transformer.estArrLt, err = parseDateTime(lastStringElement(transformer.estArrDateTimeLts))
	checkError(err)

	transformer.actDepLt, err = parseDateTime(lastStringElement(transformer.actDepDateTimeLts))
	checkError(err)

	transformer.actArrLt, err = parseDateTime(lastStringElement(transformer.actArrDateTimeLts))
	checkError(err)
	return transformer
}

func parseDateAndTime(d string, t string) (time.Time, error) {
	dateTimeString := d + space + t
	return parseDateTime(dateTimeString)
}

func parseDateTime(dt string) (time.Time, error) {
	if 0 == len(dt) {
		return zeroDate, nil
	}
	return time.Parse(dateTimeLayout, dt)
}

func Etl(ctx context.Context, pool *pgxpool.Pool, chunk []Source, createdAt time.Time) {
	tx, err := pool.Begin(ctx)
	checkError(err)
	for _, flight := range chunk {
		dateTime, err := parseDateAndTime(flight.schdDepOnlyDateLt, flight.schdDepOnlyTimeLt)
		checkError(err)

		sources := Sources(ctx, pool, flight)
		transformer := transform(sources)

		targetId, err := targetId(ctx, pool, flight.flightIcaoCode, flight.flightNumber, dateTime)
		if nil == err {
			tx.Exec(ctx, updateSQL,
				targetId,
				lastStringElement(transformer.depAptCodeIatas), lastStringElement(transformer.arrAptCodeIatas),
				lastStringElement(transformer.flightIcaoCodes), lastStringElement(transformer.flightNumbers),
				lastStringElement(transformer.carrierIcaoCodes), lastStringElement(transformer.carrierNumbers),
				lastStringElement(transformer.statusInfos),
				lastTimeElement(transformer.schdDepLts), lastTimeElement(transformer.schdArrLts),
				transformer.estDepLt, transformer.estArrLt,
				transformer.actDepLt, transformer.actArrLt,
				transformer.fltLegSeqNo,
				lastStringElement(transformer.aircraftNameScheduleds),
				reverseJoin(transformer.baggageInfos),
				reverseJoin(transformer.counters),
				reverseJoin(transformer.gateInfos),
				reverseJoin(transformer.loungeInfos),
				reverseJoin(transformer.terminalInfos),
				reverseJoin(transformer.arrTerminalInfos),
				lastStringElement(transformer.sourceDatas),
				createdAt)
		} else if noRowsInRS == err.Error() {
			tx.Exec(ctx, insertSQL,
				lastStringElement(transformer.depAptCodeIatas), lastStringElement(transformer.arrAptCodeIatas),
				lastStringElement(transformer.flightIcaoCodes), lastStringElement(transformer.flightNumbers),
				lastStringElement(transformer.carrierIcaoCodes), lastStringElement(transformer.carrierNumbers),
				lastStringElement(transformer.statusInfos),
				lastTimeElement(transformer.schdDepLts), lastTimeElement(transformer.schdArrLts),
				transformer.estDepLt, transformer.estArrLt,
				transformer.actDepLt, transformer.actArrLt,
				transformer.fltLegSeqNo,
				lastStringElement(transformer.aircraftNameScheduleds),
				reverseJoin(transformer.baggageInfos),
				reverseJoin(transformer.counters),
				reverseJoin(transformer.gateInfos),
				reverseJoin(transformer.loungeInfos),
				reverseJoin(transformer.terminalInfos),
				reverseJoin(transformer.arrTerminalInfos),
				lastStringElement(transformer.sourceDatas),
				createdAt)
		} else {
			checkError(err)
		}
	}
	err = tx.Commit(ctx)
	checkError(err)
}

func putString(a []string, s string) []string {
	arrayLength := len(a)
	if 0 != len(strings.TrimSpace(s)) && minusSign != s && (0 == arrayLength || s != a[arrayLength-1]) {
		a = append(a, s)
	}
	return a
}

func putTime(a []time.Time, t time.Time) []time.Time {
	arrayLength := len(a)
	if 0 == arrayLength || t != a[arrayLength-1] {
		a = append(a, t)
	}
	return a
}

func lastStringElement(a []string) string {
	arrayLength := len(a)
	if 0 < arrayLength {
		return a[arrayLength-1]
	}
	return emptyString
}

func lastTimeElement(a []time.Time) time.Time {
	arrayLength := len(a)
	if 0 < arrayLength {
		return a[arrayLength-1]
	}
	return zeroDate
}

func reverseJoin(a []string) string {
	s := a[:]
	sort.Sort(sort.Reverse(sort.StringSlice(s)))
	return strings.Join(s, comma)
}
