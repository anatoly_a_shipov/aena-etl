#### Extract, Transform, Load

Ssparse data aggregation command mode application implemented in Java flavors (Spring Boot, Micronaut, Quarkus, pure JDBC), Go (pgx) and ECMAScript (pg)

#### Requirements

PostgreSQL-like RDBMS, openJDK 17

#### Configuration

PostgreSQL, user / database / password ```postgres```, running at localhost, port 5432

dockerized PostgreSQL ```./bin/docker-compose-runner.bash```

#### Record count ####

aenaflight_2017_01: 9_760_785

destination_data: 105_537

#### Performance ####

Disk fragmentation may slow ETL down, drop destination_data table and indices via: ```./bin/destination-data-and-indexes-drop.sql``` and ```destination-data-and-indexes-create.sql```

Parallel / multithreaded ETL fails in YugabyteDB & CockroachDB

Batch size: 100 records to load (100 flights)

#### License

Perl "Artistic License"
