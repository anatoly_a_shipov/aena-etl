package org.dummy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.sql.Timestamp;

public interface TargetRepository extends JpaRepository<TargetItem, Long> {

    @Query(nativeQuery = true, value = "SELECT id FROM destination_data WHERE flight_code = :flightCode AND flight_number = :flightNumber AND schd_dep_lt = :schdDepLt")
    Long getId(String flightCode, String flightNumber, Timestamp schdDepLt);
}
