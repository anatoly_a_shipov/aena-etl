package org.dummy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import jakarta.persistence.Tuple;
import java.util.List;

public interface SourceRepository extends JpaRepository<SourceItem, Long> {

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT v.flight_icao_code AS flight_icao_code, v.flight_number AS flight_number, v.schd_dep_only_date_lt AS schd_dep_only_date_lt, v.schd_dep_only_time_lt AS schd_dep_only_time_lt " +
                    "FROM aenaflight_2017_01 AS v " +
                    "WHERE schd_dep_only_date_lt IS NOT NULL AND schd_dep_only_date_lt <> '' AND schd_dep_only_time_lt IS NOT NULL AND schd_dep_only_time_lt <> ''")
    List<Tuple> getSourceGroups();

    List<SourceItem> findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLtAndSchdDepOnlyTimeLtOrderByCreatedAtAsc(String flightIcaoCode, String flightNmber, String schdDepOnlyDateLt, String schdDepOnlyTimeLt);
}
