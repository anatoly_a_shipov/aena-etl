package org.dummy;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot Main class & method.
 */
@SpringBootApplication
@SuppressWarnings("squid:RedundantThrowsDeclarationCheck")
public class Application implements CommandLineRunner {

    private final EtlService etlService;

    public Application(EtlService etlService) {
        this.etlService = etlService;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        this.etlService.etl();
    }
}
