package org.dummy.service;

import com.google.common.collect.Lists;
import io.micronaut.context.annotation.Context;
import org.dummy.domain.SourceItem;
import org.dummy.domain.TargetItem;
import org.dummy.repo.*;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.dummy.utils.DateTimeUtils.*;
import static org.dummy.utils.EmptinessUtils.isBlank;

@Context
public class EtlService {

    private static final Logger LOG = Logger.getLogger(EtlService.class.getSimpleName());
    private static final int CHUNK_SIZE = 100;
    private static final String FLIGHT_ICAO_CODE = "flight_icao_code";
    private static final String FLIGHT_NUMBER = "flight_number";
    private static final String SCHD_DEP_ONLY_DATE_LT = "schd_dep_only_date_lt";
    private static final String SCHD_DEP_ONLY_TIME_LT = "schd_dep_only_time_lt";
    private static final String EMPTY_STRING = "";
    private static final String MINUS_SIGN = "-";
    private static final String DELIMITER_COMMA = ",";

    private final SourceRepository sourceRepository;
    private final TargetRepository targetRepository;

    public EtlService(SourceRepository s, TargetRepository t) {
        this.sourceRepository = s;
        this.targetRepository = t;
    }

    public void etl() {
        long begin = System.nanoTime();
        List<Tuple> flights = this.groups();
        List<List<Tuple>> partitions = Lists.partition(flights, CHUNK_SIZE);
        Timestamp createdAt = nowTimestamp();

        for (int i = 0; i < partitions.size(); i++) {
            this.etl(partitions.get(i), i, createdAt);
        }

        LOG.log(Level.INFO, "ETL took {0} seconds", String.valueOf(seconds(begin, System.nanoTime())));
    }

    List<Tuple> groups() {
        long begin = System.nanoTime();
        List<Tuple> flights = sourceRepository.getSourceGroups();
        LOG.log(Level.INFO, "Flights retrieval took {0} seconds", String.valueOf(seconds(begin, System.nanoTime())));
        return flights;
    }

    @SuppressWarnings("java:S3776")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void etl(List<Tuple> flights, int partitionNumber, Timestamp createdAt) {
        List<TargetItem> toPersist = new ArrayList<>(0);
        long begin = System.nanoTime();
        for (Tuple flight : flights) {
            String flightIcaoCode = (String) flight.get(FLIGHT_ICAO_CODE);
            String flightNumber = (String) flight.get(FLIGHT_NUMBER);
            String schdDepOnlyDateLt = (String) flight.get(SCHD_DEP_ONLY_DATE_LT);
            String schdDepOnlyTimeLt = (String) flight.get(SCHD_DEP_ONLY_TIME_LT);
            List<SourceItem> sources =
                    this.sourceRepository
                            .findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLtAndSchdDepOnlyTimeLtOrderByCreatedAtAsc(
                                    flightIcaoCode,
                                    flightNumber,
                                    schdDepOnlyDateLt,
                                    schdDepOnlyTimeLt);
            if (!sources.isEmpty()) {
                Timestamp schdDepLt = toTimestamp(schdDepOnlyDateLt, schdDepOnlyTimeLt);
                if (null != schdDepLt) {
                    // LIFO values
                    List<String> baggageInfos = new ArrayList<>(0);
                    List<String> counters = new ArrayList<>(0);
                    List<String> gateInfos = new ArrayList<>(0);
                    List<String> loungeInfos = new ArrayList<>(0);
                    List<String> terminalInfos = new ArrayList<>(0);
                    List<String> arrTerminalInfos = new ArrayList<>(0);
                    // Latest values
                    List<String> depAptCodeIatas = new ArrayList<>(0);
                    List<String> arrAptCodeIatas = new ArrayList<>(0);
                    List<String> flightIcaoCodes = new ArrayList<>(0);
                    List<String> flightNumbers = new ArrayList<>(0);
                    List<String> carrierIcaoCodes = new ArrayList<>(0);
                    List<String> carrierNumbers = new ArrayList<>(0);
                    List<String> fltLegSeqNos = new ArrayList<>(0);
                    List<String> aircraftNameScheduleds = new ArrayList<>(0);
                    List<String> sourceDatas = new ArrayList<>(0);
                    //
                    List<Timestamp> schdDepLts = new ArrayList<>(0);
                    List<Timestamp> schdArrLts = new ArrayList<>(0);
                    List<String> statusInfos = new ArrayList<>(0);
                    List<String> estDepDateTimeLts = new ArrayList<>(0);
                    List<String> estArrDateTimeLts = new ArrayList<>(0);
                    List<String> actDepDateTimeLts = new ArrayList<>(0);
                    List<String> actArrDateTimeLts = new ArrayList<>(0);
                    for (SourceItem sourceItem : sources) {
                        // LIFO values
                        putOnTheList(baggageInfos, sourceItem.getBaggageInfo());
                        putOnTheList(counters, sourceItem.getCounter());
                        putOnTheList(gateInfos, sourceItem.getGateInfo());
                        putOnTheList(loungeInfos, sourceItem.getLoungeInfo());
                        putOnTheList(terminalInfos, sourceItem.getTerminalInfo());
                        putOnTheList(arrTerminalInfos, sourceItem.getArrTerminalInfo());
                        // Latest values
                        putOnTheList(depAptCodeIatas, sourceItem.getDepAptCodeIata());
                        putOnTheList(arrAptCodeIatas, sourceItem.getArrAptCodeIata());
                        putOnTheList(flightIcaoCodes, sourceItem.getFlightIcaoCode());
                        putOnTheList(flightNumbers, sourceItem.getFlightNumber());
                        putOnTheList(carrierIcaoCodes, sourceItem.getCarrierIcaoCode());
                        putOnTheList(carrierNumbers, sourceItem.getCarrierNumber());
                        putOnTheList(fltLegSeqNos, sourceItem.getFltLegSeqNo());
                        putOnTheList(aircraftNameScheduleds, sourceItem.getAircraftNameScheduled());
                        putOnTheList(sourceDatas, sourceItem.getSourceData());
                        //
                        putOnTheList(schdDepLts, toTimestamp(sourceItem.getSchdDepOnlyDateLt(), sourceItem.getSchdDepOnlyTimeLt()));
                        putOnTheList(schdArrLts, toTimestamp(sourceItem.getSchdArrOnlyDateLt(), sourceItem.getSchdArrOnlyTimeLt()));
                        putOnTheList(statusInfos, sourceItem.getStatusInfo());
                        putOnTheList(estDepDateTimeLts, sourceItem.getEstDepDateTimeLt());
                        putOnTheList(estArrDateTimeLts, sourceItem.getEstArrDateTimeLt());
                        putOnTheList(actDepDateTimeLts, sourceItem.getActDepDateTimeLt());
                        putOnTheList(actArrDateTimeLts, sourceItem.getActArrDateTimeLt());
                    }
                    TargetItem target = new TargetItem();
                    List<TargetItem> targetItems =
                            this.targetRepository.findByFlightCodeAndFlightNumberAndSchdDepLt(flightIcaoCode, flightNumber, schdDepLt);
                    if (!targetItems.isEmpty()) {
                        target = targetItems.get(0);
                    }
                    if (1 < targetItems.size()) {
                        LOG.info("Multiple TargetItems");
                    }
                    // LIFO values
                    target.setBaggageInfo(reverseJoin(baggageInfos));
                    target.setCounter(reverseJoin(counters));
                    target.setGateInfo(reverseJoin(gateInfos));
                    target.setLoungeInfo(reverseJoin(loungeInfos));
                    target.setTerminalInfo(reverseJoin(terminalInfos));
                    target.setArrTerminalInfo(reverseJoin(arrTerminalInfos));
                    // Latest values
                    target.setAdep(getLastElement(depAptCodeIatas))
                            .setAdes(getLastElement(arrAptCodeIatas))
                            .setFlightCode(getLastElement(flightIcaoCodes))
                            .setFlightNumber(getLastElement(flightNumbers))
                            .setCarrierCode(getLastElement(carrierIcaoCodes))
                            .setCarrierNumber(getLastElement(carrierNumbers))
                            .setFltLegSeqNo(Integer.valueOf(getLastElement(fltLegSeqNos)))
                            .setAircraftNameScheduled(getLastElement(aircraftNameScheduleds))
                            .setCreatedAt(createdAt)
                            .setSourceData(getLastElement(sourceDatas));
                    //
                    target.setSchdDepLt(latestTimestamp(schdDepLts))
                            .setSchdArrLt(latestTimestamp(schdArrLts))
                            .setStatusInfo(getLastElement(statusInfos))
                            .setEstDepLt(toTimestamp(toLocalDateTime(getLastElement(estDepDateTimeLts))))
                            .setEstArrLt(toTimestamp(toLocalDateTime(getLastElement(estArrDateTimeLts))))
                            .setActDepLt(toTimestamp(toLocalDateTime(getLastElement(actDepDateTimeLts))))
                            .setActArrLt(toTimestamp(toLocalDateTime(getLastElement(actArrDateTimeLts))));
                    toPersist.add(target);
                }
            }
        }
        LOG.log(Level.INFO, "Extract and transform {0} took {1} seconds", new Object[]{partitionNumber, String.valueOf(seconds(begin, System.nanoTime()))});
        begin = System.nanoTime();
        this.targetRepository.saveAll(toPersist);
        LOG.log(Level.INFO, "Load {0} took {1} seconds", new Object[]{partitionNumber, String.valueOf(seconds(begin, System.nanoTime()))});
    }

    static void putOnTheList(List<String> list, String s) {
        if ((null != list && !isBlank(s) && !MINUS_SIGN.equals(s)) && (list.isEmpty() || !list.get(list.size() - 1).equals(s))) {
            list.add(s);
        }
    }

    static void putOnTheList(List<Timestamp> list, Timestamp t) {
        if ((null != list && null != t) && (list.isEmpty() || !list.get(list.size() - 1).equals(t))) {
            list.add(t);
        }
    }

    static String reverseJoin(List<String> list) {
        if (!list.isEmpty()) {
            StringJoiner joiner = new StringJoiner(DELIMITER_COMMA);
            ListIterator<String> it = list.listIterator(list.size());
            String s;
            while (it.hasPrevious()) {
                s = it.previous();
                if (!isBlank(s)) {
                    joiner.add(s);
                }
            }
            return joiner.toString();
        }
        return EMPTY_STRING;
    }

    static String getLastElement(List<String> l) {
        if (null != l && !l.isEmpty()) {
            return l.get(l.size() - 1);
        }
        return EMPTY_STRING;
    }

    static Timestamp latestTimestamp(List<Timestamp> l) {
        if (null != l && !l.isEmpty()) {
            return l.get(l.size() - 1);
        }
        return null;
    }
}
