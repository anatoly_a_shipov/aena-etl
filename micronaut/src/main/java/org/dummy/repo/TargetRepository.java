package org.dummy.repo;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import org.dummy.domain.TargetItem;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface TargetRepository extends CrudRepository<TargetItem, Long> {

    List<TargetItem> findByFlightCodeAndFlightNumberAndSchdDepLt(String flightCode, String flightNumber, Timestamp schdDepLt);
}
