package org.dummy.repo;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import org.dummy.domain.SourceItem;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public
interface SourceRepository extends CrudRepository<SourceItem, Long> {

    static final String SELECT_SOURCE_GROUPS_QUERY =
            "SELECT DISTINCT v.flight_icao_code AS flight_icao_code, v.flight_number AS flight_number, v.schd_dep_only_date_lt AS schd_dep_only_date_lt, v.schd_dep_only_time_lt AS schd_dep_only_time_lt "
                    + "FROM aenaflight_2017_01 AS v "
                    + "WHERE schd_dep_only_date_lt IS NOT NULL AND schd_dep_only_date_lt <> '' AND schd_dep_only_time_lt IS NOT NULL AND schd_dep_only_time_lt <> ''";

    @Query(value = SELECT_SOURCE_GROUPS_QUERY, nativeQuery = true)
    List<Tuple> getSourceGroups();

    List<SourceItem> findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLtAndSchdDepOnlyTimeLtOrderByCreatedAtAsc(String flightIcaoCode, String flightNumber, String schdDepOnlyDateLt, String schdDepOnlyTimeLt);
}
