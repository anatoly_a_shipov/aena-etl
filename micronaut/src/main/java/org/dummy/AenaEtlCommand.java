package org.dummy;

import io.micronaut.configuration.picocli.PicocliRunner;
import org.dummy.service.EtlService;
import picocli.CommandLine.Command;
import javax.inject.Inject;

@Command(name = "aena-etl", description = "call ETL", mixinStandardHelpOptions = true)
public class AenaEtlCommand implements Runnable {

    @Inject
    private EtlService etlService;

    public static void main(String[] args) {
        PicocliRunner.run(AenaEtlCommand.class, args);
    }

    public void run() {
        this.etlService.etl();
    }
}
