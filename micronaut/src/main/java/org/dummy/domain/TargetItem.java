package org.dummy.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "destination_data")
public class TargetItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "destination_data_id_seq")
    @SequenceGenerator(name = "destination_data_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "adep", length = 8)
    private String adep;

    @Column(name = "ades", length = 8)
    private String ades;

    @Column(name = "flight_code", length = 8)
    private String flightCode;

    @Column(name = "flight_number", length = 8)
    private String flightNumber;

    @Column(name = "carrier_code", length = 8)
    private String carrierCode;

    @Column(name = "carrier_number", length = 8)
    private String carrierNumber;

    @Column(name = "status_info", length = 256)
    private String statusInfo;

    @Column(name = "schd_dep_lt")
    private Timestamp schdDepLt;

    @Column(name = "schd_arr_lt")
    private Timestamp schdArrLt;

    @Column(name = "est_dep_lt")
    private Timestamp estDepLt;

    @Column(name = "est_arr_lt")
    private Timestamp estArrLt;

    @Column(name = "act_dep_lt")
    private Timestamp actDepLt;

    @Column(name = "act_arr_lt")
    private Timestamp actArrLt;

    @Column(name = "flt_leg_seq_no")
    private Integer fltLegSeqNo;

    @Column(name = "aircraft_name_scheduled")
    private String aircraftNameScheduled;

    @Column(name = "baggage_info", length = 128)
    private String baggageInfo;

    @Column(name = "counter", length = 128)
    private String counter;

    @Column(name = "gate_info", length = 128)
    private String gateInfo;

    @Column(name = "lounge_info", length = 128)
    private String loungeInfo;

    @Column(name = "terminal_info", length = 128)
    private String terminalInfo;

    @Column(name = "arr_terminal_info", length = 128)
    private String arrTerminalInfo;

    @Column(name = "source_data")
    private String sourceData;

    @Column(name = "created_at")
    private Timestamp createdAt;

    public TargetItem() {
        //Default
    }

    public Long getId() {
        return id;
    }

    public TargetItem setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAdep() {
        return adep;
    }

    public TargetItem setAdep(String adep) {
        this.adep = adep;
        return this;
    }

    public String getAdes() {
        return ades;
    }

    public TargetItem setAdes(String ades) {
        this.ades = ades;
        return this;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public TargetItem setFlightCode(String flightCode) {
        this.flightCode = flightCode;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public TargetItem setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public TargetItem setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
        return this;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public TargetItem setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
        return this;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public TargetItem setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
        return this;
    }

    public Timestamp getSchdDepLt() {
        return schdDepLt;
    }

    public TargetItem setSchdDepLt(Timestamp schdDepLt) {
        this.schdDepLt = schdDepLt;
        return this;
    }

    public Timestamp getSchdArrLt() {
        return schdArrLt;
    }

    public TargetItem setSchdArrLt(Timestamp schdArrLt) {
        this.schdArrLt = schdArrLt;
        return this;
    }

    public Timestamp getEstDepLt() {
        return estDepLt;
    }

    public TargetItem setEstDepLt(Timestamp estDepLt) {
        this.estDepLt = estDepLt;
        return this;
    }

    public Timestamp getEstArrLt() {
        return estArrLt;
    }

    public TargetItem setEstArrLt(Timestamp estArrLt) {
        this.estArrLt = estArrLt;
        return this;
    }

    public Timestamp getActDepLt() {
        return actDepLt;
    }

    public TargetItem setActDepLt(Timestamp actDepLt) {
        this.actDepLt = actDepLt;
        return this;
    }

    public Timestamp getActArrLt() {
        return actArrLt;
    }

    public TargetItem setActArrLt(Timestamp actArrLt) {
        this.actArrLt = actArrLt;
        return this;
    }

    public Integer getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public TargetItem setFltLegSeqNo(Integer fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
        return this;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public TargetItem setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
        return this;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public TargetItem setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
        return this;
    }

    public String getCounter() {
        return counter;
    }

    public TargetItem setCounter(String counter) {
        this.counter = counter;
        return this;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public TargetItem setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
        return this;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public TargetItem setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
        return this;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public TargetItem setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
        return this;
    }

    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public TargetItem setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
        return this;
    }

    public String getSourceData() {
        return sourceData;
    }

    public TargetItem setSourceData(String sourceData) {
        this.sourceData = sourceData;
        return this;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public TargetItem setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
        return this;
    }
}
