package org.dummy.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.StringJoiner;

@Entity
@Table(name = "aenaflight_2017_01")
public class SourceItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "act_arr_date_time_lt")
    private String actArrDateTimeLt;

    @Column(name = "aircraft_name_scheduled")
    private String aircraftNameScheduled;

    @Column(name = "arr_apt_name_es")
    private String arrAptNameEs;

    @Column(name = "arr_apt_code_iata")
    private String arrAptCodeIata;

    @Column(name = "baggage_info")
    private String baggageInfo;

    @Column(name = "carrier_airline_name_en")
    private String carrierAirlineNameEn;

    @Column(name = "carrier_icao_code")
    private String carrierIcaoCode;

    @Column(name = "carrier_number")
    private String carrierNumber;

    @Column(name = "counter")
    private String counter;

    @Column(name = "dep_apt_name_es")
    private String depAptNameEs;

    @Column(name = "dep_apt_code_iata")
    private String depAptCodeIata;

    @Column(name = "est_arr_date_time_lt")
    private String estArrDateTimeLt;

    @Column(name = "est_dep_date_time_lt")
    private String estDepDateTimeLt;

    @Column(name = "flight_airline_name_en")
    private String flightAirlineNameEn;

    @Column(name = "flight_airline_name")
    private String flightAirlineName;

    @Column(name = "flight_icao_code")
    private String flightIcaoCode;

    @Column(name = "flight_number")
    private String flightNumber;

    @Column(name = "flt_leg_seq_no")
    private String fltLegSeqNo;

    @Column(name = "gate_info")
    private String gateInfo;

    @Column(name = "lounge_info")
    private String loungeInfo;

    @Column(name = "schd_arr_only_date_lt")
    private String schdArrOnlyDateLt;

    @Column(name = "schd_arr_only_time_lt")
    private String schdArrOnlyTimeLt;

    @Column(name = "source_data")
    private String sourceData;

    @Column(name = "status_info")
    private String statusInfo;

    @Column(name = "terminal_info")
    private String terminalInfo;

    @Column(name = "arr_terminal_info")
    private String arrTerminalInfo;

    @Column(name = "created_at")
    private Long createdAt;

    @Column(name = "act_dep_date_time_lt")
    private String actDepDateTimeLt;

    @Column(name = "schd_dep_only_date_lt")
    private String schdDepOnlyDateLt;

    @Column(name = "schd_dep_only_time_lt")
    private String schdDepOnlyTimeLt;

    public SourceItem() {
        //Default
    }

    public Long getId() {
        return id;
    }

    public SourceItem setId(Long id) {
        this.id = id;
        return this;
    }

    public String getActArrDateTimeLt() {
        return actArrDateTimeLt;
    }

    public SourceItem setActArrDateTimeLt(String actArrDateTimeLt) {
        this.actArrDateTimeLt = actArrDateTimeLt;
        return this;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public SourceItem setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
        return this;
    }

    public String getArrAptNameEs() {
        return arrAptNameEs;
    }

    public SourceItem setArrAptNameEs(String arrAptNameEs) {
        this.arrAptNameEs = arrAptNameEs;
        return this;
    }

    public String getArrAptCodeIata() {
        return arrAptCodeIata;
    }

    public SourceItem setArrAptCodeIata(String arrAptCodeIata) {
        this.arrAptCodeIata = arrAptCodeIata;
        return this;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public SourceItem setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
        return this;
    }

    public String getCarrierAirlineNameEn() {
        return carrierAirlineNameEn;
    }

    public SourceItem setCarrierAirlineNameEn(String carrierAirlineNameEn) {
        this.carrierAirlineNameEn = carrierAirlineNameEn;
        return this;
    }

    public String getCarrierIcaoCode() {
        return carrierIcaoCode;
    }

    public SourceItem setCarrierIcaoCode(String carrierIcaoCode) {
        this.carrierIcaoCode = carrierIcaoCode;
        return this;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public SourceItem setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
        return this;
    }

    public String getCounter() {
        return counter;
    }

    public SourceItem setCounter(String counter) {
        this.counter = counter;
        return this;
    }

    public String getDepAptNameEs() {
        return depAptNameEs;
    }

    public SourceItem setDepAptNameEs(String depAptNameEs) {
        this.depAptNameEs = depAptNameEs;
        return this;
    }

    public String getDepAptCodeIata() {
        return depAptCodeIata;
    }

    public SourceItem setDepAptCodeIata(String depAptCodeIata) {
        this.depAptCodeIata = depAptCodeIata;
        return this;
    }

    public String getEstArrDateTimeLt() {
        return estArrDateTimeLt;
    }

    public SourceItem setEstArrDateTimeLt(String estArrDateTimeLt) {
        this.estArrDateTimeLt = estArrDateTimeLt;
        return this;
    }

    public String getEstDepDateTimeLt() {
        return estDepDateTimeLt;
    }

    public SourceItem setEstDepDateTimeLt(String estDepDateTimeLt) {
        this.estDepDateTimeLt = estDepDateTimeLt;
        return this;
    }

    public String getFlightAirlineNameEn() {
        return flightAirlineNameEn;
    }

    public SourceItem setFlightAirlineNameEn(String flightAirlineNameEn) {
        this.flightAirlineNameEn = flightAirlineNameEn;
        return this;
    }

    public String getFlightAirlineName() {
        return flightAirlineName;
    }

    public SourceItem setFlightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
        return this;
    }

    public String getFlightIcaoCode() {
        return flightIcaoCode;
    }

    public SourceItem setFlightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public SourceItem setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public String getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public SourceItem setFltLegSeqNo(String fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
        return this;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public SourceItem setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
        return this;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public SourceItem setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
        return this;
    }

    public String getSchdArrOnlyDateLt() {
        return schdArrOnlyDateLt;
    }

    public SourceItem setSchdArrOnlyDateLt(String schdArrOnlyDateLt) {
        this.schdArrOnlyDateLt = schdArrOnlyDateLt;
        return this;
    }

    public String getSchdArrOnlyTimeLt() {
        return schdArrOnlyTimeLt;
    }

    public SourceItem setSchdArrOnlyTimeLt(String schdArrOnlyTimeLt) {
        this.schdArrOnlyTimeLt = schdArrOnlyTimeLt;
        return this;
    }

    public String getSourceData() {
        return sourceData;
    }

    public SourceItem setSourceData(String sourceData) {
        this.sourceData = sourceData;
        return this;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public SourceItem setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
        return this;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public SourceItem setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
        return this;
    }

    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public SourceItem setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
        return this;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public SourceItem setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getActDepDateTimeLt() {
        return actDepDateTimeLt;
    }

    public SourceItem setActDepDateTimeLt(String actDepDateTimeLt) {
        this.actDepDateTimeLt = actDepDateTimeLt;
        return this;
    }

    public String getSchdDepOnlyDateLt() {
        return schdDepOnlyDateLt;
    }

    public SourceItem setSchdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
        return this;
    }

    public String getSchdDepOnlyTimeLt() {
        return schdDepOnlyTimeLt;
    }

    public SourceItem setSchdDepOnlyTimeLt(String schdDepOnlyTimeLt) {
        this.schdDepOnlyTimeLt = schdDepOnlyTimeLt;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SourceItem that = (SourceItem) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SourceItem.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("actArrDateTimeLt='" + actArrDateTimeLt + "'")
                .add("aircraftNameScheduled='" + aircraftNameScheduled + "'")
                .add("arrAptNameEs='" + arrAptNameEs + "'")
                .add("arrAptCodeIata='" + arrAptCodeIata + "'")
                .add("baggageInfo='" + baggageInfo + "'")
                .add("carrierAirlineNameEn='" + carrierAirlineNameEn + "'")
                .add("carrierIcaoCode='" + carrierIcaoCode + "'")
                .add("carrierNumber='" + carrierNumber + "'")
                .add("counter='" + counter + "'")
                .add("depAptNameEs='" + depAptNameEs + "'")
                .add("depAptCodeIata='" + depAptCodeIata + "'")
                .add("estArrDateTimeLt='" + estArrDateTimeLt + "'")
                .add("estDepDateTimeLt='" + estDepDateTimeLt + "'")
                .add("flightAirlineNameEn='" + flightAirlineNameEn + "'")
                .add("flightAirlineName='" + flightAirlineName + "'")
                .add("flightIcaoCode='" + flightIcaoCode + "'")
                .add("flightNumber='" + flightNumber + "'")
                .add("fltLegSeqNo='" + fltLegSeqNo + "'")
                .add("gateInfo='" + gateInfo + "'")
                .add("loungeInfo='" + loungeInfo + "'")
                .add("schdArrOnlyDateLt='" + schdArrOnlyDateLt + "'")
                .add("schdArrOnlyTimeLt='" + schdArrOnlyTimeLt + "'")
                .add("sourceData='" + sourceData + "'")
                .add("statusInfo='" + statusInfo + "'")
                .add("terminalInfo='" + terminalInfo + "'")
                .add("arrTerminalInfo='" + arrTerminalInfo + "'")
                .add("createdAt=" + createdAt)
                .add("actDepDateTimeLt='" + actDepDateTimeLt + "'")
                .add("schdDepOnlyDateLt='" + schdDepOnlyDateLt + "'")
                .add("schdDepOnlyTimeLt='" + schdDepOnlyTimeLt + "'")
                .toString();
    }
}
