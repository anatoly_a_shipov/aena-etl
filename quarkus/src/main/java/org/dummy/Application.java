package org.dummy;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.dummy.service.EtlService;
import javax.enterprise.context.control.ActivateRequestContext;

/**
 * App.
 */
@QuarkusMain
public class Application implements QuarkusApplication {

    private final EtlService etlService;

    /**
     * Constructor.
     * @param etlService {@link EtlService}
     */
    public Application(EtlService etlService) {
        this.etlService = etlService;
    }

    @ActivateRequestContext
    @Override
    public int run(String... args) throws Exception {
        this.etlService.etl();
        return 0;
    }
}
