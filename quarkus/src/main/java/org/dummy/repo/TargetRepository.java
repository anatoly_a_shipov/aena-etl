package org.dummy.repo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.dummy.domain.TargetItem;
import javax.enterprise.context.ApplicationScoped;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class TargetRepository implements PanacheRepository<TargetItem> {

    public List<TargetItem> findByFlightCodeAndFlightNumberAndSchdDepLt(String flightCode, String flightNumber, Timestamp schdDepLt) {
        Map<String, Object> params = new HashMap<>();
        params.put("flightCode", flightCode);
        params.put("flightNumber", flightNumber);
        params.put("schdDepLt", schdDepLt);
        return list(
                "SELECT ti FROM TargetItem AS ti WHERE ti.flightCode = :flightCode AND ti.flightNumber = :flightNumber AND ti.schdDepLt = :schdDepLt",
                params
        );
    }
}
