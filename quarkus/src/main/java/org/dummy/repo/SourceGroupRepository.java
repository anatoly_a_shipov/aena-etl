package org.dummy.repo;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import java.util.List;

@ApplicationScoped
public class SourceGroupRepository {

    private static final String SELECT_SOURCE_GROUPS_QUERY =
            "SELECT DISTINCT v.flight_icao_code AS flight_icao_code, v.flight_number AS flight_number, v.schd_dep_only_date_lt AS schd_dep_only_date_lt, v.schd_dep_only_time_lt AS schd_dep_only_time_lt "
                    + "FROM aenaflight_2017_01 AS v "
                    + "WHERE schd_dep_only_date_lt IS NOT NULL AND schd_dep_only_date_lt <> '' AND schd_dep_only_time_lt IS NOT NULL AND schd_dep_only_time_lt <> ''";

    private final EntityManager em;

    public SourceGroupRepository(EntityManager em) {
        this.em = em;
    }

    public List<Tuple> getSourceGroups() {
        return this.em.createNativeQuery(SELECT_SOURCE_GROUPS_QUERY, Tuple.class).getResultList();
    }
}
