package org.dummy.repo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.dummy.domain.SourceItem;
import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SourceRepository implements PanacheRepository<SourceItem> {

    public List<SourceItem> findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLtAndSchdDepOnlyTimeLtOrderByCreatedAtAsc(String flightIcaoCode, String flightNumber, String schdDepOnlyDateLt, String schdDepOnlyTimeLt) {
        Map<String, Object> params = new HashMap<>();
        params.put("flightIcaoCode", flightIcaoCode);
        params.put("flightNumber", flightNumber);
        params.put("schdDepOnlyDateLt", schdDepOnlyDateLt);
        params.put("schdDepOnlyTimeLt", schdDepOnlyTimeLt);
        return list(
                "SELECT si FROM SourceItem AS si WHERE si.flightIcaoCode = :flightIcaoCode AND si.flightNumber = :flightNumber AND si.schdDepOnlyDateLt = :schdDepOnlyDateLt AND si.schdDepOnlyTimeLt = :schdDepOnlyTimeLt",
                params
        );
    }
}
